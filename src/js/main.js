// Андрей Алексеев [AA]
// alexeev.andrey.a@gmail.com
$.fn.shake = function(intShakes, intDistance, intDuration) {
  this.each(function() {
      $(this).css("position","relative"); 
      for (var x=1; x<=intShakes; x++) {
      $(this).animate({left:(intDistance*-1)}, (((intDuration/intShakes)/4)))
  .animate({left:intDistance}, ((intDuration/intShakes)/2))
  .animate({left:0}, (((intDuration/intShakes)/4)));
  }
  });
return this;
};

// jquery document ready
$(function() {

 //smoothscroll
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 800);
        return false;
      }
    }
  });

  // инит слайдера
  if ($('.iosslider').length) {
    slider.init();
  }

  // инит хедера
  if ($('.header').length) {
    header.init();
  }

  // инит блока с работами
  if ($('#works').length) {
    works.init();
  }

  if ($('#timeline').length) {
    timeline.init();
  }

  if ($('.modal').length) {
    modal.init();
  }

  // init placeholder
  $('input, textarea').placeholder();

  if ($('.js-form').length) {
    form.init();
  }

  // if ($('.progress').length) {
  //   progress.init();
  // }

});
// END doc.ready