// популярные команды
// для сокращений проставь алиас grunt --> g
// grunt build (g b) - собрать проект в public/ и загрузить на гугл диск
// grunt watch (g w)
// grunt clean:empty - первоначальная чистка
// grunt imageoptim - для png
// grunt imagemin - для jpg
// grunt sprite - собрать иконки в спрайт

module.exports = function(grunt) {

  // Тут мы указываем Grunt, что нужно подгрузить задания
  require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});
  require('time-grunt')(grunt);

  // 1. Вся настройка находится здесь
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //конфиги папок
    config: {
      src: 'src',
      dist: 'public',
      gdrive: 'public'
      // gdrive: '../../Google Диск/<%= pkg.name %>/www/'
    },

    //конкатенация файлов
    //jquery находится в папке lib, но не подключается
    concat: {
      // options: {
      //   separator: ';'
      // },
      dist: {
        src: [
          '<%= config.src %>/js/lib/transition.js',
          '<%= config.src %>/js/lib/modal.js',
          '<%= config.src %>/js/lib/jquery.iosslider.min.js',
          '<%= config.src %>/js/lib/jquery.placeholder.js',
          // '<%= config.src %>/js/lib/jquery.maskedinput.min.js',
          '<%= config.src %>/js/lib/jquery-scrollspy.js',
          // '<%= config.src %>/js/lib/retina.min.js',
          '<%= config.src %>/js/lib/isotope.pkgd.js',
          '<%= config.src %>/js/app/*.js',
          '<%= config.src %>/js/main.js'
          ],    
        dest: '<%= config.src %>/js/build/scripts.js'
      }
    },

    //минификация
    uglify: {
      options: {
        //добавляем дату компиляции
        banner: '/*\nAuthor: <%= pkg.author.name %> \nEmail: <%= pkg.author.email %> \n<%= pkg.name %> build <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: '<%= config.src %>/js/build/scripts.js',
        dest: '<%= config.src %>/js/build/scripts.min.js'
      }
    },

    //боль и страдания
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        },
      },
      src: ['<%= config.src %>/js/main.js']
    },

    //сжатие изображений
    imagemin: {
      stuff: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/files/',
          // src: ['**/*.{png,jpg,gif}'],
          src: ['**/*.{jpg,gif}'],
          dest: '<%= config.src %>/files/'
        }]
      },

      images: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/img/',
          // src: ['**/*.{png,jpg,gif}'],
          src: ['**/*.{jpg,gif}'],
          dest: '<%= config.src %>/img/'
        }]
      }
    }, 

    imageoptim: {
      options: {
        quitAfter: true
      },
      allPngs: {
        options: {
          imageAlpha: true,
          jpegMini: false
        },
        src: ['<%= config.src %>/img/**/*.png', '<%= config.src %>/files/**/*.png']
      }
      //},
      // платно
      // allJpgs: {
      //   options: {
      //     imageAlpha: false,
      //     jpegMini: true
      //   },
      //   src: ['<%= config.src %>/img/**/*.jpg', '<%= config.src %>/files/**/*.jpg']
      // }
    },

    //компилятор лесс
    less: {
      dist: {
        options: {
          // cleancss:"true"
        },

        src: '<%= config.src %>/less/style.less',
        dest: '<%= config.src %>/css/style.css'
      }
    },

    cmq: {
      files: {
        src: '<%= config.src %>/css/style.css',
        dest: '<%= config.src %>/css/'
      }
    },

    //префиксы
    autoprefixer: {
      single_file: {
        options: {
          // Target-specific options go here.
          browsers: ['last 3 versions', '> 1%', 'ie 8', 'ie 9', 'Opera 12.1']
        },
        src: '<%= config.src %>/css/style.css'
      },
    },

    //css
    cssmin: {
      options: {
        banner: '/*\nAuthor: <%= pkg.author.name %> \nEmail: <%= pkg.author.email %> \n<%= pkg.name %> build <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      dist: {
        src: '<%= config.src %>/css/style.css',
        dest: '<%= config.src %>/css/style.min.css'
      }
    },

    //отслеживание
    watch: {

      //скрипты минифицировать и подключать
      scripts: {
          files: ['<%= config.src %>/js/**/*.js'],
          tasks: ['concat', 'uglify', 'notify:uglify'],
          options: {
              spawn: false,
          },
      },

      //лесс компилировать, префиксовать и подключать
      less: {
        files: ['<%= config.src %>/less/**/*.less'],
        tasks: ['less', 'notify:less', 'cmq', 'autoprefixer', 'cssmin'],
        // tasks: ['less', 'autoprefixer', 'cssmin'],
        options: {
            spawn: false,
            livereload: true
        }
      },

      livereload: {
        options: { livereload: true },
        files: ['<%= config.src %>/**/*.html','<%= config.src %>/css/**/*.css','<%= config.src %>/js/**/*.js']
      }
    },

    //копирование в папку public
    copy: {
      js: {
        files: [
          { expand: true, 
            cwd: '<%= config.src %>/js/build/', 
            src: 'scripts.min.js', 
            dest: '<%= config.dist %>/js/build/'
          },
    //jQuery!
          {
            src: '<%= config.src %>/js/lib/jquery-1.11.1.min.js',
            dest: '<%= config.dist %>/js/lib/jquery-1.11.1.min.js'
            // dest: '<%= config.dist %>/js/lib/'
          },
          {
            src: '<%= config.src %>/js/lib/html5shiv.js',
            dest: '<%= config.dist %>/js/lib/html5shiv.js'
            // dest: '<%= config.dist %>/js/lib/'
          },
    //Modernizr
          {
            expand: true,
            cwd: '<%= config.src %>/js/lib',
            src: 'modernizr*',
            dest: '<%= config.dist %>/js/lib/'
          }
        ],
      },
      svg: {
        expand: true,
        cwd: '<%= config.src %>/svgicon',
        src: '**/*',
        dest: '<%= config.dist %>/svgicon'
      },
      css: {
        expand: true,
        cwd: '<%= config.src %>/css',
        src: ['*.css','!style*', 'style.min.css'],
        dest: '<%= config.dist %>/css'
      },
      fonts: {
        expand: true,
        cwd: '<%= config.src %>/fonts',
        src: '*.{eot,svg,ttf,woff}',
        dest: '<%= config.dist %>/fonts/'
      },
      stuff: {
        expand: true,
        cwd: '<%= config.src %>',
        //файлы, начинающиеся с !, копии не подлежат
        src: ['!!**/*','!**/!*','*.{html,png,ico,txt,php}', 'files/**/*','fonts/**/*','img/**/*'],
        dest: '<%= config.dist %>'
      },
      gdrive: {
        expand: true,
        cwd: '<%= config.dist %>',
        src: ['**/*'],
        dest: '<%= config.gdrive %>'
      }
    },

    notify: {
      less: {
        options: {
          title: 'Готово!',  // optional
          message: 'LESS файл скомпилирован', //required
        }
      },

      uglify: {
        options: {
          title: 'Готово!',  // optional
          message: 'JS собран, друже!', //required
        }
      }
    },

    // чистка файлов
    // фикс от гита
    clean: {
      empty: ['**/_EMPTY.txt'],
      svg: '<%= config.src %>/svgicon',
      dist: '<%= config.dist %>',
      // release: ['<%= config.dist %>/**/*']
      release: ['<%= config.dist %>', '<%= config.gdrive %>']
    }, 

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/!svg-src',
          src: ['!!ai','*.svg'],
          dest: '<%= config.src %>/svgicon/svgs'
        }]
      }
    },

    grunticon: {
      mysvg: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/svgicon/svgs',
          src: ['*.svg', '*.png'],
          dest: '<%= config.src %>'
        }],
        options: {
          datasvgcss: 'css/grunticon-icons.data.svg.css',
          datapngcss: 'css/grunticon-icons.data.png.css',
          urlpngcss: 'css/grunticon-icons.fallback.css',
          previewhtml: '!grunticon-preview.html',
          pngfolder: 'svgicon/png-grunticon',
          pngpath: '../svgicon/png-grunticon',
          loadersnippet:'svgicon/grunticon.loader.js',
          template: '<%= config.src %>/!svg-src/!grunticon-template.hbs',
          defaultWidth: '100px',
          defaultHeight: '100px',
          dynamicColorOnly: true,
          colors: {
              white: "#ffffff",
              blue: "#007aff"
          },
          customselectors: {
            // "logo-white":[".icon-logo"],
            // "logo-black":[".icon-logo-fixed"],
            // "logo-blue":[".main-logo:hover .icon-logo-fixed", ".main-logo:active .icon-logo-fixed"],
            // "logo-mobile":[".icon-logo-mobile"],
            "ftr-logo":[".icon-ftr-logo:before",".icon-ftr-logo:active:before"],
            "ftr-logo-hvr":[".icon-ftr-logo:hover:before"],
            "close-mobile":[".header__hamburger-btn:after"],
            "btn-close":[".btn-close:before",".btn-close:active:before "],
            "btn-close-hvr":[".btn-close:hover:before "],
            "ftr-arr":[".icon-ftr-arr:before"],
            "ftr-arr-hvr":[".icon-ftr-arr:hover:before",".icon-ftr-arr:active:before"],
            "ftr-left-arr":[".footer__arrow--left:before",".footer__nav-item:active .footer__arrow--left:before"],
            "ftr-left-arr-hvr":[".footer__nav-item:hover .footer__arrow--left:before"],
            "ftr-right-arr":[".footer__arrow--right:before",".footer__nav-item:active .footer__arrow--right:before"],
            "ftr-right-arr-hvr":[".footer__nav-item:hover .footer__arrow--right:before"],
            "map-pointer":[".map-pointer"],
            "inst":[".icon-inst:before",".icon-inst:active:before"],
            "inst-hvr":[".icon-inst:hover:before"]
          }
        }
      }
    }
  });

  // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
  grunt.registerTask('default', ['concat','uglify','imagemin','imageoptim','less','cmq','autoprefixer','cssmin']);
  grunt.registerTask('js', ['concat','uglify']);
  grunt.registerTask('css', ['less','autoprefixer','cssmin']);
  grunt.registerTask('svg', ['clean:svg','svgmin','grunticon']);
  grunt.registerTask('pain', ['jshint']);
  grunt.registerTask('build', ['concat','uglify','imagemin','imageoptim','less','cmq','autoprefixer','cssmin','copy']);
  grunt.registerTask('b', ['concat','uglify','less','autoprefixer','cssmin','copy']);
  grunt.registerTask('w', ['watch']);
};